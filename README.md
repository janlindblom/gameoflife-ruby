# GameOfLife

A simple implementation of Conway's Game of Life in Ruby.

Comes in two flavours:

- a rather simple, pretty straighforward version, and
- an improved version, that uses bundler and rubygems plus concurrency.

### Simple Version

Checkout branch `simple_version` and run with:

    $ ruby -I. game_of_life.rb

### Improved Version

Checkout branch `improved_version` and run with:

    $ bundle install
    $ bundle exec bin/gameoflife